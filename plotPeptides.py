#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 10:47:48 2017

@author: wichmann

installation for windows 
download python from 
https://www.python.org/downloads/release/python-371/

install (as administrator) into c:\python\python37

run cmd as administrator 

cd c:\Python\Python37\Scripts
pip install matplotlib
pip install scipy
pip install pandas

script can then be run by 
c:\Python\Python37\python.exe plotMassError.py logfile1.txt [....]


"""


import pandas as pd
import sys
from scipy import *
from scipy import ndimage
from numpy import *
from pylab import *
from matplotlib import *
import pylab

def density(x,y,res=100,s=3):
    xedges, yedges = linspace(x.min(), x.max(), res), linspace(y.min(), y.max(), res)
    hist, xedges, yedges = histogram2d(x, y, (xedges, yedges))
    xidx = clip(digitize(x, xedges), 0, hist.shape[0]-1)
    yidx = clip(digitize(y, yedges), 0, hist.shape[1]-1)
    hist=ndimage.filters.gaussian_filter(hist,3)
    c=hist[xidx, yidx]
    return c

def parseLogFile( fName ):

    fl=open(fName)
    data=[ i.split()[5:] for i in fl.readlines() if i.find("FOUND")!=-1 ]
    fl.close()

    keys,idz,values=['id:'],[],[]
    for c,s in enumerate(data[0]):
    	if( s[-1]==":"): keys.append( s )

    for s in data:
        v=[s[0]]
        for c,i in enumerate(s):
        	if( i[-1]==":"): v.append(float(s[c+1]))
        values.append(v)

    peptides =  pd.DataFrame(values,columns=keys)
    peptideLengths = peptides.groupby('id:')['rt:'].max()-peptides.groupby('id:')['rt:'].min()

    peptides = peptides.loc[ peptides.groupby('id:')['intensity:'].idxmax() ]
    peptides['length:'] = peptideLengths.values

    return peptides.sort_values('rt:')

# plot
def plotPeptides(fp):
	m           = fp['mz:']
	me          = fp['mz_correction:']
	err         = fp['mz_window:']
	off         = fp['mz_deviation:']

	t           = fp['rt:']
	t_diff      = fp['rt_deviation:']
	t_me        = fp['rt_correction:']
	t_err       = fp['rt_window:']

	int_ratio   = fp['int_ratio:']
	int_corr    = fp['int_correction:']
	lengths     = fp['length:']


	f=1.0e6

	rc('figure', figsize=(20,20))

	suptitle("%i peptides found [%s]; median length [%5.2f]"% (len(t) ,fName,median(lengths)))

	subplot(331)

	fill_between(t,(me)/m*f-err,(me)/m*f+err,color="gray",alpha=0.5)

	x,y=t, off/m*f
	title("Mass deviation, median=%5.2f"% (median(abs(y))) )
	scatter(x, y,c=density(x,y), marker=".",linewidth=0)
	#plot( t, off/m*f,"o",ms=3.0,color="black")
	plot( t, me/m*f ,"-",lw=1.5,color="black")
	#plot(t,t*0.0,".",lw=0.5,color="black")
	xlim([min(t)-1,max(t)+1])
	ylabel("ppm")
	xlabel("min")


	subplot(334)

	fill_between(t,-err,err,color="gray",alpha=0.5)
	x,y=t, (off-me)/m*f
	title("Corrected mass deviation, median=%5.2f"% (median(abs(y))) )
	scatter(x, y,c=density(x,y), marker=".",linewidth=0)
	#plot(t, (off-me)/m*f,"o",ms=3.0,color="black")
	plot( t, (me-me)/m*f ,"-",lw=1.5,color="black")
	plot(t,t*0.0,"-",lw=0.5,color="black")
	xlim([min(t)-1,max(t)+1])
	ylabel("ppm")
	xlabel("min")


	subplot(337)
	hist( array((off-me)/m*f), 25,range=(-5,5),color="gray")
	xlabel("ppm")
	xlim(-5,5)

	subplot(332)
	title("Retention time deviation, median=%5.2f"% (median(abs(t_diff))))
	fill_between(t,t_me-0.5*t_err,t_me+0.5*t_err,color="gray",alpha=0.5)
	plot(t,t*0.0,"-",lw=0.5,color="black")
	#plot(t, t_diff,"o",ms=3.0,color="black")



	scatter(t, t_diff,c=density(t,t_diff), marker=".",linewidth=0)
	plot( t, t_me ,"-",lw=1.5,color="black")

	ylabel("min")
	xlabel("min")
	#xlim([min(t)-1,max(t)+1])


	subplot(335)
	title("corrected retention time deviation, median=%5.2f"% (median(abs(t_diff-t_me))))
	fill_between(t,-0.5*t_err,0.5*t_err,color="gray",alpha=0.5)
	plot(t,t*0.0,"-",lw=0.5,color="black")

	scatter(t, t_diff-t_me,c=density(t,t_diff-t_me), marker=".",linewidth=0)
	#plot(t, t_diff-t_me,"o",ms=3.0,color="black")

	plot( t, t_me-t_me ,"-",lw=1.50,color="black")

	ylabel("min")
	xlabel("min")
	#xlim([min(t)-1,max(t)+1])

	subplot(338)
	hist( array(t_diff), 50,range=(-5,5),color="lightgray",alpha=0.5)
	hist( array(t_diff-t_me), 50,range=(-5,5),color="gray",alpha=0.5)
	xlim(-5,5)
	xlabel("min")


	subplot(333)
	x,y=t, log(int_ratio)
	title("intensity ratios, median=%5.2f"% (median(abs(y))))

	scatter(x, y,c=density(x,y), marker=".",linewidth=0)
	plot(t, log(1.0/int_corr),"-",lw=1.5,color="black")
	ylabel("")
	xlabel("min")


	subplot(336)

	x,y=t, log(int_ratio*int_corr)
	title("corrected intensity ratios, median=%5.2f"% (median(abs(y))))
	scatter(x, y,c=density(x,y), marker=".",linewidth=0)
	plot(t, t*0.0,"-",ms=3.0,color="gray")
	ylabel("")
	xlabel("min")

	subplot(339)
	hist( array(int_ratio*int_corr), 25,range=(0,5),color="gray")
	xlim(0,5)
	xlabel("")

	savefig("peptides_%s.pdf" % fName)
	savefig("peptides_%s.png" % fName)
	fp.to_csv("peptides_%s.txt" % fName )

	
	
	
	
for fName in sys.argv[1:]:
	fp = parseLogFile( fName )
	plotPeptides(fp)


	